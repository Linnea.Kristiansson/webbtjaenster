package com.example.groupapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroupapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroupapiApplication.class, args);
	}

}
